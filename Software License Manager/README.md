Software License Manager 

Original author: Joshua Carroll [about.me/joshucarroll](http://about.me/joshuacarroll)
[Project website](https://bitbucket.org/joshuacarroll/software-license-manager)
[Issue tracker](https://bitbucket.org/joshuacarroll/software-license-manager/issues?status=new&status=open)


The Software License Manager is a tool to help programmers who license software or SAAS to protect their intellectual property easily - without having to write their own license key management system into their applications. It is written in C# and based on the Windows Communication Foundation (WCF) framework, which means that it can be consumed from any platform (client, server, Windows, *nix, etc).

