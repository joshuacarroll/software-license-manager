﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace Software_License_Manager
{
    [ServiceContract]
    public interface ISoftwareLicenseManager
    {
        [OperationContract]
        bool LicenseKeyIsValid(string licenseKey, string applicationName);

        [OperationContract]
        DateTime GetLicenseExpirationDate(string licenseKey, string applicationName);
    }   

    //[DataContract]
    //public class CompositeType
    //{
    //    bool boolValue = true;
    //    string stringValue = "Hello ";

    //    [DataMember]
    //    public bool BoolValue
    //    {
    //        get { return boolValue; }
    //        set { boolValue = value; }
    //    }

    //    [DataMember]
    //    public string StringValue
    //    {
    //        get { return stringValue; }
    //        set { stringValue = value; }
    //    }
    //}
}
