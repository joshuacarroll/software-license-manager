﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Data.SqlClient;
using System.Web.UI.WebControls;

namespace Software_License_Manager
{
    public class SoftwareLicenseManager : ISoftwareLicenseManager
    {
        public bool LicenseKeyIsValid(string licenseKey, string applicationName)
        {
            bool rtnValue = false; // Prove me wrong

            ///TODO: Open database connection from Web.config
            string connectionString = System.Web.Configuration.WebConfigurationManager.ConnectionStrings["LicenseDatabaseConnectionString"].ConnectionString;
            
            string query = string.Format("SELECT LicenseID FROM Licenses WHERE LicenseString = '{0}' AND ApplicationNameString = '{1}' AND ExpirationDate >= '{2}';", licenseKey, applicationName, DateTime.Now);

            SqlConnection conn = new SqlConnection(connectionString);
            SqlCommand cmd = new SqlCommand(query, conn);
            
            ///TODO: Send query asking about whether or not the license is valid
            object scalar = cmd.ExecuteScalar();

            if (scalar != null)
            {
                rtnValue = true;
            }

            return rtnValue;
        }

        public DateTime GetLicenseExpirationDate(string licenseKey, string applicationName)
        {
            DateTime rtnValue = new DateTime(1815, 12, 10); // Again... prove me wrong.

            ///TODO: Open database connection from Web.config

            ///TODO: Send query asking about whether or not the license is valid, and when it expires.

            return rtnValue;
        }
    }
}
